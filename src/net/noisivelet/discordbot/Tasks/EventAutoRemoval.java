/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Tasks;

import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Database;
import static net.noisivelet.discordbot.DiscordBot.exception;
import static net.noisivelet.discordbot.DiscordBot.log;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;

/**
 *
 * @author Francis
 */
public class EventAutoRemoval implements Runnable {

    Event e;
    

    public EventAutoRemoval(Event e) {
        this.e = e;
    }
    
    @Override
    public void run() {
        try{
            MessageChannel channel=null;
            if(e.isPublished()){
                channel=e.getPublishedMessage().getChannel();
            }
            Guild g=e.guild;
            e.wipe();
            if(e.isPeriodic()){
                e.setStart(new Date(e.getStart().getTime()+e.offsetPeriodic()));
                if(channel!=null){
                    e.publish(channel);
                }
                log("Event "+e.getName()+" was automatically re-created after "+Event.AUTODELETE_DELAY+" ms.");
                return;
            }
            e.delete();
            log("Event "+e.getName()+" was automatically deleted after "+Event.AUTODELETE_DELAY+" ms.");
        } catch (Exception e){
            e.printStackTrace();
        }
        
    }
    
}
