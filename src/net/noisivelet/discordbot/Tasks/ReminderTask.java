/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Tasks;

import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.exception;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Util.Event.Attendant;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class ReminderTask implements Runnable{
    private final Event e;
    public ReminderTask(Event e){
        this.e=e;
    }

    @Override
    public void run() {
        try{
            log("Starting reminder task for event "+e.getName()+" - Guild "+e.guild.getName());
            Date start=e.getStart();
            String date=new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.UK).format(start);
            String hour=new SimpleDateFormat("HH:mm").format(start);
            if(!e.isPublished()) return;
            MessageBuilder builder=new MessageBuilder().setEmbed(new EmbedBuilder()
                    .setTitle("Event starting soon")
                    .setDescription("The following event is starting in less than 10 minutes:")
                    .setColor(new Color(8311585))
                    .setAuthor(e.creator.getUser().getName(), null, e.creator.getUser().getAvatarUrl())
                    .addField("**Name**", e.getName(), false)
                    .addField("**Description**", e.getDescription(), false)
                    .addField("**Starting date**", date+" "+hour, false)
                    .build());
            String mentions="";
            int quantity;
            if(e.getN_attendants()==-1)
                quantity=e.getAttendants().size();
            else
                quantity=e.getN_attendants()<e.getAttendants().size()?e.getN_attendants():e.getAttendants().size();
            for(int i=0;i<quantity;i++){
                mentions+=e.getAttendants().get(i).member.getAsMention()+" ";
            }
            log("Sending message to "+quantity+" members.");
            builder.append(mentions);
            Message global=builder.build();
            Utils.sendAndDelete(global, e.getPublishedMessage().getChannel(), 20*60);
            for(int i=0;i<quantity;i++){
                Attendant a=e.getAttendants().get(i);
                Member m1=a.member;
                PrivateChannel pc = m1.getUser().openPrivateChannel().complete();
                Message mess=new MessageBuilder().setEmbed(new EmbedBuilder()
                        .setTitle("Your event starts soon")
                        .setDescription("The event you joined will begin in less than 10 minutes. Get ready!\nYou can find more info about which event I'm talking about below:")
                        .setColor(new Color(8311585))
                        .setAuthor(e.creator.getUser().getName(), null, e.creator.getUser().getAvatarUrl())
                        .addField("**Name**", e.getName(), false)
                        .addField("**Description**", e.getDescription(), false)
                        .addField("**Starting date**", date+" "+hour, false)
                        .build())
                        .build();
                pc.sendMessage(mess).submit();
            }
        } catch (Exception ex){
            exception(ex);
        }
    }
}
