/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Tasks;

import net.noisivelet.discordbot.DiscordBot;
import java.util.concurrent.TimeUnit;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

/**
 *
 * @author Francis
 */
public class MessageDeleter implements Runnable{
    
    private final Message m;
    private final MessageChannel c;
    private final int duration;
    
    public MessageDeleter(Message m, MessageChannel c, int duration){
        this.m=m;
        this.c=c;
        this.duration=duration;
    }
    
    @Override
    public void run() {
        try{
            Message message=c.sendMessage(m).complete();
            DiscordBot.SCHEDULER.schedule(() -> {
                message.delete().submit();
            }, duration, TimeUnit.SECONDS);
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
}
