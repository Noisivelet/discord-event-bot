/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Listeners;

import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Util.Event.Attendant;
import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 *
 * @author Francis
 */
public class ReactionManager extends ListenerAdapter{
    
    private final ConcurrentHashMap<Guild, ArrayList<Event>> events;

    public ReactionManager(ConcurrentHashMap<Guild, ArrayList<Event>> events) {
        this.events = events;
    }
    
    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event){
        if(event.getUser().isBot()) return;
        ArrayList<Event> server=events.get(event.getGuild());
        
        if(server==null) return;
        
        for(int i=0;i<server.size();i++){
            Event e=server.get(i);
            if(e.isPublished())
                if(e.getPublishedMessage().getId().equals(event.getMessageId())){
                    Emote emote;
                    try{
                        emote=event.getReactionEmote().getEmote();
                    } catch(IllegalStateException ex){
                        if(event.getReactionEmote().getName().equals(Event.EMOTE_WAITING)){
                            waitingList(event,e);
                        }
                        return;
                    }
                    if(emote.equals(Event.EMOTE_JOIN)){
                        addMember(event,e);
                    } else if(emote.equals(Event.EMOTE_LEAVE)) {
                        removeMember(event,e);
                    }
                }
        }
    }

    private void addMember(MessageReactionAddEvent event, Event e) {
        if(e.hasStarted()){
            e.clearReactions();
            return;
        }
        Member m=event.getMember();
        if(!e.hasAttendant(m)){
            e.addAttendant(m);
            log(event.getUser().getName()+"#"+event.getUser().getDiscriminator()+" joined the event \""+e.getName()+"\" (From guild \""+e.guild.getName()+"\")");
        } else {
            e.clearReactions();
        }
    }

    private void removeMember(MessageReactionAddEvent event, Event e) {
        if(e.hasStarted()){
            e.clearReactions();
            return;
        }
        Member m=event.getMember();
        if(!e.hasDenial(m)){
            e.addDenial(m);
            log(event.getUser().getName()+"#"+event.getUser().getDiscriminator()+" added to denials in event \""+e.getName()+"\" (From guild \""+e.guild.getName()+"\")");
        } else {
            e.clearReactions();
        }
        
    }

    private void waitingList(MessageReactionAddEvent event, Event e) {
        
        if(e.getN_attendants() == -1) return;
        if(e.getAttendants().size()<=e.getN_attendants()) return;
        User u=event.getUser();
        DiscordBot.EXECUTOR.submit(() -> {
            PrivateChannel pc=u.openPrivateChannel().complete();
            String list="";
            ArrayList<Attendant> attendants=e.getAttendants();
            for(int i=e.getN_attendants();i<attendants.size();i++){
                list+=i+") "+attendants.get(i).member.getAsMention();
                if(i!=attendants.size()-1){
                    list+="\n";
                }
            }
            String plural=attendants.size()-e.getN_attendants()==1?"member":"members";
            Message m=new MessageBuilder()  .setEmbed(new EmbedBuilder()
                .setTitle("Event attendants: "+e.getName())
                .setDescription("Note that this list is inmutable and won't update automatically. If you need a new, updated list, press the :clock10: button once again.\n[To return to the original message card, click here.](https://discordapp.com/channels/"+e.guild.getId()+"/"+e.getPublishedMessage().getChannel().getId()+"/"+e.getPublishedMessage().getId()+")")
                .setColor(new Color(4886754))
                .setFooter("United as One EventBot | Created by @Noisivelet#0001", "https://cdn.discordapp.com/attachments/624926218512367616/668416919967236097/UAO.png")
                .setAuthor(e.creator.getUser().getName(), null, e.creator.getUser().getAvatarUrl())
                .addField("Event's waiting list **["+(attendants.size()-e.getN_attendants())+" "+plural+"]:**", list, false)
                .addBlankField(false)
                .build())
            .build();
            pc.sendMessage(m).submit();
            log(event.getUser().getName()+"#"+event.getUser().getDiscriminator()+" received the waiting list of event "+e.getName()+"(Guild "+e.guild.getName()+")");
            e.clearReactions();
        });
    }
}
