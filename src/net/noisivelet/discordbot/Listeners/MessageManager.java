/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Listeners;

import net.noisivelet.discordbot.Util.Database;
import static net.noisivelet.discordbot.DiscordBot.log;
import java.util.ArrayList;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.noisivelet.discordbot.Util.Command;
import net.noisivelet.discordbot.Util.EventCreationManager;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class MessageManager extends ListenerAdapter{
    @Override
    public void onMessageReceived(MessageReceivedEvent event){
        if(!event.isFromType(ChannelType.TEXT)) return;
        //if(event.getGuild().getIdLong()!=653597084318171166L) return;
        if(!Database.EVENTS.containsKey(event.getGuild())){
            Database.EVENTS.put(event.getGuild(), new ArrayList<>());
        }
        Message message=event.getMessage();
        String rawMessage=message.getContentRaw();
        EventCreationManager e=Database.getEventCreationManager(event.getMember());
        if(e != null){
            e.parse(message, event.getChannel());
            return;
        }
        if(!rawMessage.startsWith("!")) return; //prefix
        String[] args=rawMessage.split(" ");
        Command cmd=Utils.COMMAND_MAP.getOrDefault(args[0], null);
        if(cmd==null)
            return;
        cmd.execute(message, args, event.getMember(), event.getChannel());
        String log_msg=event.getAuthor().getName()+"#"+event.getAuthor().getDiscriminator()+" issued a command: "+cmd.getCommand();
        log(log_msg);
    }
}