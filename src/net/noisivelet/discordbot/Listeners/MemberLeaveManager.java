/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Listeners;

import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Util.Event.Attendant;
import java.util.ArrayList;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.noisivelet.discordbot.Util.Database;

/**
 *
 * @author Francis
 */
public class MemberLeaveManager extends ListenerAdapter{
    
    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event){
        Guild g=event.getGuild();
        if(!Database.EVENTS.containsKey(g)){
            Database.EVENTS.put(g, new ArrayList<>());
        }
        log(event.getUser().getName()+"#"+event.getUser().getDiscriminator()+" left the guild \""+event.getGuild().getName()+"\". Removing from all joined events...");
        Database.EVENTS.get(g).forEach((e) -> {
            ArrayList<Attendant> attendants=e.getAttendants();
            for(int i=0;i<attendants.size();i++){
                Member m=attendants.get(i).member;
                if(m.getIdLong()==event.getMember().getIdLong()){
                    e.removeAttendant(i);
                    log(event.getUser().getName()+"#"+event.getUser().getDiscriminator()+" removed from event \""+e.getName()+"\".");
                    i=attendants.size();
                }
            }
        });
        
        
    }
}
