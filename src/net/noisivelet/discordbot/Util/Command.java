/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.Commands.CommandExecutor;
import org.jetbrains.annotations.NotNull;

/**
 *
 * @author Francis
 */
public class Command {
    private String command;
    private String args;
    private String usage;
    private final CommandExecutor executor;
    private List<String> aliases;
    private boolean isInternal=false; //Internal commands won't appear on !help

    public void setInternal(boolean isInternal) {
        this.isInternal = isInternal;
    }
    
    public boolean isInternal(){
        return isInternal;
    }

    public Command(String command, @NotNull String args, String usage, List<String> aliases, CommandExecutor executor) {
        this.command = command;
        this.args = args;
        this.usage = usage;
        this.executor=executor;
        if(aliases==null)
            this.aliases=new ArrayList<>();
        else
            this.aliases=aliases;
    }
    
    public Object[] getAliases(){
        return aliases.toArray();
    }
    
    public final void execute(Message msg, String[] args, Member m, MessageChannel channel){
        executor.execute(msg,args,m,channel);
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }
    
    @Override
    public String toString(){
        return command+" "+args+" - "+usage;
    }
}
