/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import java.awt.Color;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.error;
import static net.noisivelet.discordbot.DiscordBot.exception;
import static net.noisivelet.discordbot.DiscordBot.log;
import static net.noisivelet.discordbot.Util.Utils.errorMessage;
import static net.noisivelet.discordbot.Util.Utils.successMessage;

/**
 *
 * @author Francis
 */
public class EventCreationManager {

    public void parse(Message message, MessageChannel channel) {
        ecs.messages.add(message);
        String[] args=message.getContentRaw().split(" ");
        if("cancel".equals(message.getContentRaw())){
            cancelCreation(channel);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" cancelled the creation of their event.");
            return;
        }
        switch(ecs.state){
            case TYPE:
                type(args, channel);
                return;
            case NAME:
                name(args, channel);
                return;
            case DESCRIPTION:
                desc(args, channel);
                return;
            case ATTENDANTS:
                atts(args, channel);
                return;
            case DATE:
                date(args, channel);
                return;
            default:
                error("Undefined behaviour @ EventCreationManager::parse");
        }
    }

    private void finishEvent(MessageChannel channel) {
        Guild guild=ecs.editing.guild;
        User author=m.getUser();
        Database.EVENTS.get(guild).add(ecs.editing);
        int event_id=Database.EVENTS.get(guild).size()-1;
        EmbedBuilder embed=new EmbedBuilder()
            .setTitle("Event created successfully")
            .setDescription("The event has been created.\nThe ID of the created event is [**"+event_id+"**]. Remember it.\nFor more commands, you can type `!uhelp`.")
            .setColor(new Color(8311585))
            .setAuthor(author.getName(), null, author.getAvatarUrl());
        Configuration config=Database.getConfig(guild);
        MessageChannel autopub=config.getAuto_publish();
        if(autopub==null)
            embed.addField("Publishing the event", "Type the command\n`!eventpublish "+event_id+"`,\nthat will publish the event in the actual channel.", true);
        else{
            ecs.editing.publish(autopub);
            embed.addField("Auto-publish", "The event has been automatically published in the channel <#"+autopub.getId()+">.\nYou can advertise it in another channel by typing `!eventpublish "+event_id+"` there.", true);
        }
        Message msg=new MessageBuilder().setEmbed(embed.build()).build();
        Utils.sendAndDelete(msg, channel, 15);
        ecs.messages.forEach((mes) -> {
            DiscordBot.EXECUTOR.submit(() -> {
                mes.delete().submit();
            });
        });
        Database.EVENTCREATE_MEMORY.get(guild).remove(this.m);
        try {
            ecs.editing.addToDatabase();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" finished the creation of a new event.");
    }

    private void cancelCreation(MessageChannel channel) {
        Database.EVENTCREATE_MEMORY.get(m.getGuild()).remove(m);
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Cancelling...")
            .setDescription("Changed your mind? Very well, I've deleted the event for you.")
            .setColor(new Color(8311585))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
          .build();
        Utils.sendAndDelete(msg, channel, 10);
        ecs.messages.forEach((mes) -> {
            DiscordBot.EXECUTOR.submit(() -> {
                mes.delete().submit();
            });
        });
    }
    
    static public class EventCreateState{
        public Event editing;
        public State state;
        public ArrayList<Message> messages;
        public enum State{
            TYPE, NAME, DESCRIPTION, ATTENDANTS, DATE, CONTEST_END, CONTEST_MAXVOTES, CONTEST_CHAT
        }
        public EventCreateState(Event editing, State state){
            this.editing=editing;
            this.state=state;
            messages=new ArrayList<>();
        }
    }
    
    private EventCreateState ecs;
    private Member m;
    public EventCreationManager(Member m){
        this.m=m;
        ecs=new EventCreateState(new Event(m.getGuild(), m), EventCreateState.State.TYPE);
    }
    public void addMessage(Message m){
        ecs.messages.add(m);
    }
    
    
    
    private void type(String[] args, MessageChannel channel){
        if(args.length==1){
            String name;
            try{
                Integer type=Integer.parseInt(args[0]);
                switch(type){
                    case 1:
                        ecs.editing=new Event(m.getGuild(), m);
                        ecs.state=EventCreateState.State.NAME;
                        name="NORMAL";
                        
                        break;
                    case 2:
                        sendMessage(errorMessage("Event creation - NYI", "This option is not yet implemented. Coming soon!", m), channel);
                        return;
                        /*ecs.editing=new ContestEvent(m.getGuild(), m);
                        ecs.state=EventCreateState.State.NAME;
                        name="CONTEST";
                        break;*/
                    default:
                        throw new NumberFormatException();
                }
                sendMessage(successMessage("Event creation - Type set", "Type of event correctly set to "+name+".\n\n**Type the name of the event.**",m), channel);

            } catch(NumberFormatException e){
                ecs.editing=new Event(m.getGuild(), m);
                ecs.editing.setName(args[0]);
                ecs.state=EventCreateState.State.DESCRIPTION;
                sendMessage(successMessage("Event creation - Skipping type selector", "Skipped the type selection process.\nType of event set to NORMAL. Event name correctly set.\n**__Type the description of the event. What will the event consist of?__**",m),channel);
            }
        } else {
            ecs.editing=new Event(m.getGuild(), m);
            String name="";
            for(int i=0;i<args.length;i++){
                name+=args[i];
                    if(i!=args.length-1)
                        name+=" ";
            }
            ecs.editing.setName(name);
            ecs.state=EventCreateState.State.DESCRIPTION;
            sendMessage(successMessage("Event creation - Skipping type selector", "Skipped the type selection process.\nType of event set to NORMAL. Event name correctly set.\n**__Type the description of the event. What will the event consist of?__**",m),channel);

        }
    }
    
    
    private void name(String[] args, MessageChannel channel) {
        String message="";
        for(int i=0;i<args.length;i++){
            message+=args[i];
            if(i<args.length-1){
                message+=" ";
            }
        }
        ecs.editing.setName(message);
        ecs.state=EventCreateState.State.DESCRIPTION;
        sendMessage(successMessage("Event creation - Name set", "Name correctly set.\n**__Type the description of the event. What will your event consist of?__**",m), channel);
        
    }
    
    private void desc(String[] args, MessageChannel channel) {
        String desc="";
        for(int i=0;i<args.length;i++){
            desc+=args[i];
            if(i<args.length-1)
                desc+=" ";
        }
        ecs.editing.setDescription(desc);
        ecs.state=EventCreateState.State.DATE;
        sendMessage(successMessage("Event creation - Description set","Description of the event set.\n\n**When will the event start? Type the date and time, using the following format:\n`DD-MM-YYYY HH:mm`**\n\n*For example: 23-02-2020 19:00 would equal to the 23th of February, 2020 at 19:00 (Server time)*",m), channel);
    }
    
    private void sendMessage(Message m, MessageChannel c){
        ecs.messages.add(c.sendMessage(m).complete());
    }
    
    private void date(String[] args, MessageChannel channel) {
        String date="";
        for(int i=0;i<args.length;i++){
            date+=args[i];
            if(i<args.length-1)
                date+=" ";
        }
        Date d;
        try {
            d=new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(date);
        } catch (ParseException ex) {
            sendMessage(errorMessage("Editing event - Incorrect date", "The date specified is not in the valid format.\n\n**The date has to be typed using the following format:\n`DD-MM-YYYY HH:mm`**\n\n*For example: 23-02-2020 19:00 would equal to the 23th of February, 2020 at 19:00 (Server time)*",m), channel);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used an invalid date. Their format: "+date);
            return;
        }
        
        long now=Instant.now().getEpochSecond()*1000;
        if(d.getTime() < now){
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used a date before now.");
            sendMessage(errorMessage("Event creation - Incorrect date", "The start date should be after now. The specified date has already passed.\n\n**Type the date and time, using the following format:\n`DD-MM-YYYY HH:mm`**\n\n*For example: 23-02-2020 19:00 would equal to the 23th of February, 2020 at 19:00 (Server time)*",m), channel);
            return;
        }
        ecs.editing.setStart(d);
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" changed the starting date of the event they were creating to "+date);
        
        if(!(ecs.editing instanceof ContestEvent)){
            ecs.state=EventCreateState.State.ATTENDANTS;
            sendMessage(successMessage("Event creation - Date set", "Start date successfully recorded.\n**Type (with a number), the maximum number of attendants this event will have.**\n*Tip: Type `-1` if you want this event to have infinite attendants.*",m), channel);
        } else {
            ecs.state=EventCreateState.State.CONTEST_END;
            sendMessage(successMessage("Event creation - Start date set", "Start date successfully recorded.\n**Type, using the same `DD-MM-YYYY HH:mm` format, the end date of the contest event.** After this date arrives, the channel will be locked and no one will be able to vote anymore. The results will then be posted in the event card.",m), channel);
        }
        return;
    }
    
    private void atts(String[] args, MessageChannel channel) {
        String attendants=args[0];
        int a;
        try{
            a=Integer.parseInt(attendants);
        } catch(NumberFormatException ex){
            sendMessage(errorMessage("Creating new event - Attendants", "Error parsing attendant number. Try again.\n\n**Type (with a number), the maximum number of attendants this event will have.**",m), channel);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" typed an invalid number. Their argument: "+attendants);
            return;
        }
        
        if(a==0||a<-1){
            sendMessage(errorMessage("Creating new event - Attendants","Error parsing attendant number: An event must have, at the very least, space for a member.\n\n**Type (with a number), the maximum number of attendants this event will have.**",m), channel);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used an invalid number of members. Their number of members: "+attendants);
            return;
        }
        ecs.editing.setN_attendants(a);
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" changed the number of attendants for the event "+ecs.editing.getName()+" to "+attendants);
        finishEvent(channel);
    }
}

