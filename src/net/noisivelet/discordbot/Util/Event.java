/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import net.noisivelet.discordbot.Tasks.EventAutoRemoval;
import net.noisivelet.discordbot.Tasks.ReminderTask;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Database.EventField;
import static net.noisivelet.discordbot.DiscordBot.exception;
import java.awt.Color;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.managers.RoleManager;
import static net.noisivelet.discordbot.DiscordBot.log;

/**
 *
 * @author Francis
 */
public class Event {
    public static class Attendant implements Comparable{
        public Member member;
        public Date joindate;
        
        public Attendant(Member member, Date joindate) {
            this.member = member;
            this.joindate = joindate;
        }

        @Override
        public int compareTo(Object t) {
            if(!(t instanceof Attendant)){
                return -1;
            }
            Attendant o=(Attendant)t;
            long thisdate=joindate.getTime();
            long otherdate=o.joindate.getTime();
            if(thisdate<otherdate) return -1;
            if(thisdate==otherdate) return 0;
            return 1;
        }
    }
    public static final int ATTENDANTS=0;
    public static final int DENIALS=1;
    public static Emote EMOTE_JOIN=DiscordBot.jda.getEmoteById(607624611563307008L);
    public static Emote EMOTE_LEAVE=DiscordBot.jda.getEmoteById(607624536992776209L);
    public static String EMOTE_WAITING="🕙";
    public static long AUTODELETE_DELAY=1000; //milliseconds
    private String name, description;
    private Integer n_attendants;
    private Date start;
    private ArrayList<Attendant> attendants, denials;
    public final Guild guild;
    public final Member creator;
    private Message publishedMessage;
    private long dbid;
    private ScheduledFuture reminder, autoRemoval, startTask;
    private boolean periodic;
    private long offsetPeriodic;
    private boolean showEmptySpots;
    private Role role;

    public Event(Guild guild, Member creator){
        this(null, null, null, null, guild, creator, 0);
    }
    
    public boolean hasStarted(){
        return System.currentTimeMillis()>=start.getTime();
    }
    
    private Event(String name, String description, Integer n_attendants, Date start, Guild guild, Member creator, long role_id) {
        this.name = name;
        this.description = description;
        this.n_attendants = n_attendants;
        this.start = start;
        this.guild = guild;
        this.creator = creator;
        attendants=new ArrayList<>();
        denials=new ArrayList<>();
        publishedMessage=null;
        dbid=-1;
        this.periodic=false;
        offsetPeriodic=-1;
        autoRemoval=null;
        showEmptySpots=false;
        startTask=null;
        if(role_id==0){
            role=null;
        } else {
            this.role=guild.getJDA().getRoleById(role_id);
        }
    }
    
    private void createRole(){
        if(role!=null)
            return;
        if(!canBePublished())
            return;
        role=guild.createRole().complete();
        RoleManager manager=role.getManager();
        manager.setName("Event: "+name).setPermissions(new ArrayList<>()).submit();
    }

    public Event(HashMap<String, String> row, ArrayList<Attendant> attendants, ArrayList<Attendant> denials) {
        this(
            row.get("name"),
            row.get("description"),
            Integer.parseInt(row.get("n_attendants")),
            new Date(Long.parseLong(row.get("start"))),
            DiscordBot.jda.getGuildById(row.get("guild")),
            DiscordBot.jda.getGuildById(row.get("guild")).getMemberById(row.get("creator")),
            Long.parseLong(row.get("role"))
        );
        dbid=Long.parseLong(row.get("id"));
        if(row.get("role").equals("0")){
            createRole();
            try {
                Database.updateEvent(this, EventField.ROLE);
            } catch (SQLException ex) {
                exception(ex);
            }
        }
        String message=row.get("message");
        if(message!=null){
            String channel=row.get("message_channel");
            try{
                publishedMessage=guild.getTextChannelById(channel).retrieveMessageById(message).complete();
            } catch (Exception e){
                publishedMessage=null;
            }
            
        }
        periodic=!row.get("periodic").equals("0");
        if(periodic)
            offsetPeriodic=Long.parseLong(row.get("offsetPeriodic"));
        this.attendants=attendants;
        this.denials=denials;
        
        showEmptySpots=!row.get("showEmptySpots").equals("0");
        startReminder();
        reScheduleAutoRemoval();
    }

    public Role getRole() {
        return role;
    }
    
    public void setShowEmptySpots(boolean val){
        if(showEmptySpots==val)return;
        showEmptySpots=val;
        updatePublishedMessage();
        try {
            Database.updateEvent(this, EventField.EMPTY_SPOTS);
        } catch (SQLException ex) {
            exception(ex);
        }
    }
    
    public boolean showEmptySpots(){return showEmptySpots;}

    public boolean isPeriodic() {
        return periodic;
    }

    public long offsetPeriodic(){
        if(!periodic)
            return -1;
        else return offsetPeriodic; 
    }
    
    public void setPeriodic(long offset) {
        if(offset==-1){
            this.periodic=false;
            try {
                Database.updateEvent(this, EventField.PERIODIC);
            } catch (SQLException ex) {
                exception(ex);
            }
            return;
        }
        this.periodic=true;
        this.offsetPeriodic=offset;
        updatePublishedMessage();
        try {
            Database.updateEvent(this, EventField.PERIODIC);
        } catch (SQLException ex) {
            exception(ex);
        }
    }
    
    
    public long getDBId(){
        return dbid;
    }
    public void addToDatabase() throws SQLException{
        if(!canBePublished())return;
        
        dbid=Database.queryInsert("INSERT INTO events VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?)", creator.getUser().getId(), guild.getId(), name, description, (n_attendants+""), (start.getTime()+""), publishedMessage==null?null:publishedMessage.getId(), (publishedMessage==null)?null:publishedMessage.getChannel().getId(), periodic?"1":"0", periodic?offsetPeriodic+"":null, showEmptySpots?"1":"0",role==null?"0":role.getId());
    }

    public boolean canBePublished(){
        return (name!=null && description!=null && n_attendants!=null && start!=null);
    }
    
    public ArrayList<Attendant> getAttendants(){
        return attendants;
    }
    
    public ArrayList<Attendant> getDenials(){
        return denials;
    }
    
    public boolean hasAttendant(Member e){
        return(has(ATTENDANTS, e));
    }
    
    public boolean hasDenial(Member e){
        return(has(DENIALS, e));
    }
    
    private boolean has(int type, Member e){
        ArrayList<Attendant> target;
        if(type==ATTENDANTS)
            target=attendants;
        else
            target=denials;
        for(int i=0;i<target.size();i++){
            if(target.get(i).member.equals(e))
                return true;
        }
        return false;
    }
    private void add(int type, Member e){
        ArrayList<Attendant> target, aux;
        if(type==ATTENDANTS){
            target=attendants;
            aux=denials;
        } else {
            target=denials;
            aux=attendants;
        }
        if(has(type==ATTENDANTS?DENIALS:ATTENDANTS, e)){
            for(int i=0;i<aux.size();i++){
                if(aux.get(i).member.equals(e)){
                    aux.remove(i);
                    i=aux.size();
                }
            }
            Date now=new Date();
            target.add(new Attendant(e, now));
            try {
                Database.queryDML("UPDATE event_attendants SET denial=? WHERE id_event=? AND attendant=?", type+"", dbid+"", e.getUser().getId());
            } catch (SQLException ex) {
                exception(ex);
            }
            updatePublishedMessage();
            return;
        }
        Date now=new Date();
        Attendant a=new Attendant(e, now);
        target.add(a);
        try {
            Database.queryInsert("INSERT INTO event_attendants VALUES (?,?,?,?)", dbid+"", e.getUser().getId(), now.getTime()+"", type+"");
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
    }
    public void addDenial(Member e){
        add(DENIALS, e);
    }
    public void addAttendant(Member e){
        add(ATTENDANTS, e);
    }
    
    public void removeAttendant(int index){
        if(index>attendants.size()-1){
            throw new ArrayIndexOutOfBoundsException("Index "+index+" out of range, attendant size: "+attendants.size());
        }
        Member e=attendants.remove(index).member;
        try {
            Database.queryDML("DELETE FROM event_attendants WHERE id_event=? AND attendant=?", dbid+"", e.getUser().getId());
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
        
    }
    
    public void removeAttendant(Member e){
        for(int i=0;i<attendants.size();i++){
            if(attendants.get(i).member.equals(e)){
                removeAttendant(i);
                return;
            }
        }
        throw new IllegalStateException("The selected member is not in this event.");
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        try {
            Database.updateEvent(this,EventField.NAME);
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        try {
            Database.updateEvent(this,EventField.DESCRIPTION);
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
    }

    public int getN_attendants() {
        return n_attendants;
    }

    public void setN_attendants(int n_attendants) {
        this.n_attendants = n_attendants;
        try {
            Database.updateEvent(this,EventField.ATTENDANTS);
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
        try {
            Database.updateEvent(this, EventField.DATE);
        } catch (SQLException ex) {
            exception(ex);
        }
        updatePublishedMessage();
        stopReminder();
        startReminder();
        reScheduleAutoRemoval();
    }
    
    private void reScheduleAutoRemoval(){
        if(autoRemoval!=null){
            autoRemoval.cancel(false);
        }
        long starting=start.getTime();
        long now=System.currentTimeMillis();
        starting+=Event.AUTODELETE_DELAY;
        EventAutoRemoval task=new EventAutoRemoval(this);
        autoRemoval=DiscordBot.SCHEDULER.schedule(task, starting-now, TimeUnit.MILLISECONDS);
    }
    
    public void publish(MessageChannel channel) throws IllegalStateException{
        if(!canBePublished())
            throw new IllegalStateException("The event hasn't been completed yet.");
        createRole();
        Message m=asMessage();
        removePublish();
        try {
            publishedMessage=channel.sendMessage(m).submit().get();
            clearReactions();
        } catch (InterruptedException | ExecutionException ex) {
            exception(ex);
        }
        try {
            Database.updateEvent(this, EventField.MESSAGE);
        } catch (SQLException ex) {
            exception(ex);
        }
    }
    
    public boolean isPublished(){
        return (publishedMessage!=null);
    }
    
    public void removePublish(){
        if(publishedMessage!=null){
            publishedMessage.delete().submit();
            publishedMessage=null;
        }
    }
    
    public Message asMessage(){
        String members="";
        int stop;
        if(n_attendants==-1)
            stop=attendants.size();
        else if(showEmptySpots)
            stop=n_attendants;
        else
            stop=attendants.size()<n_attendants?attendants.size():n_attendants;
        if(attendants.isEmpty()){
            members="*<No members have joined yet.>*\n";
        }
        for(int i=0;i<stop;i++){
            if(i>=attendants.size()){
                members+="-\n";
            } else
                members+="- "+attendants.get(i).member.getAsMention()+"\n";
        }
        int tam;
        if(n_attendants==-1)
            tam=attendants.size();
        else
            tam=attendants.size()>n_attendants?n_attendants:attendants.size();
        int waitinglist=attendants.size()>n_attendants?attendants.size()-n_attendants:0;
        String date=new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.UK).format(start);
        String hour=new SimpleDateFormat("HH:mm").format(start);
        String nickname=(creator.getNickname()==null)?creator.getUser().getName():creator.getNickname();
        String title="-- "+name+" --";
        if(periodic){
            title+=" :recycle:";
        }
        String attendant_msg;
        if(n_attendants==-1)
            attendant_msg=":white_check_mark: Attendants ["+tam+"/∞]:";
        else
            attendant_msg=":white_check_mark: Attendants ["+tam+"/"+n_attendants+"]:";
        
        String denials_msg="";
        for(int i=0;i<denials.size();i++){
            denials_msg+="- "+denials.get(i).member.getAsMention()+"\n";
        }
        if(denials.isEmpty()){
            denials_msg="*<No denials yet>*";
        }
        EmbedBuilder builder=new EmbedBuilder()
            .setTitle(title)
            .setDescription("**"+description+"**")
            .setColor(new Color(13206546))
            .setFooter("Event start time (Local time):", "https://cdn.discordapp.com/icons/594802818532114469/9d4e19c1bc55c8d78c5001f919b53067.png")
            .setTimestamp(start.toInstant())
            .setAuthor(nickname,null,creator.getUser().getAvatarUrl())
            .addField("Date & time", date+"\n"+hour+"(Server time)", true);
        if(hasStarted())
            builder.addField("Joining the event:", "The event has already started!\nyou can't join anymore.", true);
        else
            builder.addField("Joining the event:", "Click on the <:thumbsup:607624611563307008> at the bottom \nto join us. If you've changed your\nmind, click the <:thumbsdown:607624536992776209> to leave.", true);
        builder.addBlankField(false)
        .addField(attendant_msg, members, true)
        .addField(":x: Denials ["+denials.size()+"]:", denials_msg, true)
        .addBlankField(false);
        if(n_attendants!=-1)
            if(waitinglist!=0)
                builder.addField("Waiting list: "+waitinglist, "*People in the waiting list will join in order if someone leaves the main roster. Click the :clock10: below to see all members in the waiting list.*", false);
            else
                builder.addField("Waiting list: 0", "*People in the waiting list will join in order if someone leaves the main roster.*", false);
        Message m=new MessageBuilder()
        //.append("@everyone")
        .setEmbed(builder.build())
        .build();
        
        return m;
    }
    
    @Override
    public String toString(){
        String date=new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.UK).format(start);
        String hour=new SimpleDateFormat("HH:mm").format(start);
        String str="**"+getName()+"**- ["+attendants.size()+"/"+n_attendants+"] "+date+" "+hour;
        return str;
    }
    
    public Message getPublishedMessage(){
        return publishedMessage;
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Event)) return false;
        return o.hashCode()==this.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.name);
        hash = 23 * hash + Objects.hashCode(this.description);
        hash = 23 * hash + Objects.hashCode(this.n_attendants);
        hash = 23 * hash + Objects.hashCode(this.start);
        return hash;
    }

    public void updatePublishedMessage() {
        if(!isPublished()) return;
        createRole();
        Message m=asMessage();
        attendants.forEach((a) -> {
            guild.addRoleToMember(a.member, role).submit();
        });
        denials.forEach((a) -> {
            guild.removeRoleFromMember(a.member, role).submit();
        });
        try{
            publishedMessage.editMessage(m).submit().get();
            clearReactions();
        } catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
        }
    }
    
    public void clearReactions(){
        if(!isPublished()) return;
        publishedMessage.clearReactions().complete();
        publishedMessage=publishedMessage.getChannel().retrieveMessageById(publishedMessage.getIdLong()).complete();//Updating message
        if(!hasStarted()){
            publishedMessage.addReaction(EMOTE_JOIN).complete();
            publishedMessage.addReaction(EMOTE_LEAVE).complete();
        }
        if(n_attendants!=-1&&attendants.size()>n_attendants){
            publishedMessage.addReaction(EMOTE_WAITING).complete();
        }
    }
    
    /**
     * Returns the class as a HashMap, ready to be inserted into a database.<br>
     * This doesn't have the ArrayList of attendants in consideration.
     * @return The class mapped, with each of their values converted to String, or null if the event hasn't been completely created.
     */
    @Nullable
    public HashMap<String, String> asHashMap(){
        if(!canBePublished()) return null;
        HashMap<String, String> hm=new HashMap<>();
        hm.put("creator", creator.getUser().getId());
        hm.put("guild", guild.getId());
        hm.put("name", name);
        hm.put("description", description);
        hm.put("n_attendants", n_attendants+"");
        hm.put("start", start.getTime()+"");
        hm.put("message", publishedMessage.getId());
        hm.put("message_channel", (publishedMessage==null)?null:publishedMessage.getChannel().getId());
        hm.put("periodic", periodic?"1":"0");
        hm.put("offsetPeriodic", periodic?offsetPeriodic+"":null);
        hm.put("showEmptySpots", showEmptySpots?"1":"0");
        hm.put("role", role.getId());
        return hm;
    }
    
    public boolean canDelete(Member m){
        if(m.getUser().getIdLong()==DiscordBot.CREATOR_ID) return true;
        if(m.hasPermission(Permission.ADMINISTRATOR)||m.hasPermission(Permission.MESSAGE_MANAGE)) return true;
        return m.getUser().getIdLong()==creator.getUser().getIdLong();
    }
    
    public static boolean hasFullPermissions(Member m){
        if(m.getUser().getIdLong()==DiscordBot.CREATOR_ID) return true;
        if(m.hasPermission(Permission.ADMINISTRATOR)||m.hasPermission(Permission.MESSAGE_MANAGE)) return true;
        return false;
    }
    
    public static boolean canCreate(Member m){
        if(m.getUser().getIdLong()==DiscordBot.CREATOR_ID) return true;
        return (m.hasPermission(Permission.ADMINISTRATOR)||m.hasPermission(Permission.MESSAGE_MENTION_EVERYONE));
    }
    
    public void stopReminder(){
        if(reminder!=null){
            reminder.cancel(false);
            reminder=null;
        }
        
        if(startTask!=null){
            startTask.cancel(false);
            startTask=null;
        }
    }
    
    public void startReminder(){
        if(reminder==null){
            long st=getStart().getTime();
            long now=Instant.now(Clock.systemUTC()).getEpochSecond()*1000;
            long time=st-now-(60*10*1000);//10 minutes earlier
            if(time > 0){
                ReminderTask reminderTask=new ReminderTask(this);
                reminder=DiscordBot.SCHEDULER.schedule(reminderTask, time, TimeUnit.MILLISECONDS);
            }
            if(st-now > 0){
                //Update event card at start time to disable join and leave reactions
                startTask=DiscordBot.SCHEDULER.schedule(() -> {
                    log("Event \""+getName()+"\" (Guild \""+guild.getName()+"\") has started.");
                    updatePublishedMessage();
                }, (st-now), TimeUnit.MILLISECONDS);
            } else {
                clearReactions();
            }
            
        }
    }
    
    public void wipe(){
        stopReminder();
        removePublish();
        for(Attendant a : attendants){
            guild.removeRoleFromMember(a.member, role).submit();
        }
        attendants.clear();
        denials.clear();
        try {
            Database.queryDML("DELETE FROM `event_attendants` WHERE id_event=?", dbid+"");
        } catch (SQLException ex) {
            exception(ex);
        }
    }
    
    public void delete(){
        wipe();
        try {
            Database.queryDML("DELETE FROM `events` WHERE id=?", dbid+"");
        } catch (SQLException ex) {
            exception(ex);
        }
        role.delete().submit();
        Database.EVENTS.get(guild).remove(this);
    }
    
    public void startingTask(){
        clearReactions();
    }
}
