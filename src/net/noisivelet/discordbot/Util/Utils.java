/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import net.noisivelet.discordbot.Commands.EventDelete;
import net.noisivelet.discordbot.Commands.EventPublish;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.Commands.AutoPublish;
import net.noisivelet.discordbot.Commands.Boop;
import net.noisivelet.discordbot.Commands.EventCreate;
import net.noisivelet.discordbot.Commands.EventEdit;
import net.noisivelet.discordbot.Commands.EventList;
import net.noisivelet.discordbot.Commands.Help;
import net.noisivelet.discordbot.Commands.Stop;
import net.noisivelet.discordbot.Commands.UpdateCards;
import static net.noisivelet.discordbot.DiscordBot.EXECUTOR;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Tasks.MessageDeleter;

/**
 *
 * @author Francis
 */
public abstract class Utils {
    public final static List<Command> COMMAND_LIST=getCommandList();
    public final static ConcurrentHashMap<String, Command> COMMAND_MAP=mapCommandList();

    private static List<Command> getCommandList(){
        ArrayList<Command> commands=new ArrayList<>();
        
        //Commands
        commands.add(new Command("!eventcreate", "", "Creates a new event. The bot will guide you through the process. You will receive an event ID at the end. Save it in case you need to edit the event later.", null, new EventCreate()));
        ArrayList<String> advertise_aliases=new ArrayList<>();
        advertise_aliases.add("!eventadvertise");
        commands.add(new Command("!eventpublish", "<id>", "Publishes the event with the ID <id> in the current channel. Have in consideration that if an event is already published, publishing it again will delete the original event card (No changes will be made to the event itself).", advertise_aliases, new EventPublish()));
        commands.add(new Command("!eventedit", "<id> <parameter> <new value>", "Edits the event with the ID <id>.\n<parameter> can be one of the following: `name, desc, date, attendants, periodic, emptyslots`", null, new EventEdit()));
        commands.add(new Command("!eventdelete", "<id>", "Deletes the event with the ID <id>. Be careful, a deleted event can't be recovered.", null, new EventDelete()));
        ArrayList<String> autopublish_aliases=new ArrayList<>();
        autopublish_aliases.add("!autoadvertise");
        commands.add(new Command("!autopublish", "[#message_channel]", "Activates or deactivates the autopublish feature, depending on whether or not you specify an argument. Autopublish enables the bot to advertise an event automatically after its created.", autopublish_aliases, new AutoPublish()));
        commands.add(new Command("!boop", "", "Tests the bot. It will reply with a reaction if its working.", null, new Boop()));
        commands.add(new Command("!eventlist", "", "Gives a list of all currently planned events. Events that have started, but not finished, will be marked as :warning:. Events that will repeat after done are marked as :recycle:. Contest events are marked as :sparkles:.", null, new EventList()));
        commands.add(new Command("!help", "", "The command you just entered. Shows a card with all available commands and their general usage.", null, new Help()));
        Command stop=new Command("!stop", "", "Stops the bot. Only a developer can use this.", null, new Stop());
        stop.setInternal(true);
        commands.add(stop);
        Command updateCards=new Command("!updatecards", "", "Updates all event cards to their new format for this server. Only a developer can use this.", null, new UpdateCards());
        updateCards.setInternal(true);
        commands.add(updateCards);
        
        
        return commands;
    }
    
    public static void sendAndDelete(Message e, MessageChannel c, int time){
        EXECUTOR.submit(new MessageDeleter(e, c, time));
    }
    
    public static Message errorMessage(String title, String message, Member m){
        return infoMessage(title, message, new Color(13632027), m);
    }
    
    public static Message successMessage(String title, String message, Member m){
        return infoMessage(title, message, new Color(4886754),m);
    }
    
    public static Message infoMessage(String title, String message, Color color, Member m){
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle(title)
            .setDescription(message)
            .setColor(color)
            .setAuthor(m.getUser().getName(),null, m.getUser().getAvatarUrl())
            .build())
        .build();
        return msg;
    }

    private static ConcurrentHashMap<String, Command> mapCommandList() {
        ConcurrentHashMap<String, Command> commands=new ConcurrentHashMap<>();
        for(Command c : COMMAND_LIST){
            Object[] aliases=c.getAliases();
            for(Object alias_obj:aliases){
                String alias=(String)alias_obj;
                commands.put(alias, c);
            }
            commands.put(c.getCommand(), c);
        }
        return commands;
    }

    public static Message insufficientPermissionsEdit(Member m, String action) {
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" lacks the permissions to edit the event.");
        Message msg=new MessageBuilder()
        .setEmbed(new EmbedBuilder()
          .setTitle("Error executing command")
          .setDescription("You lack the permissions to "+action+" this event.")
          .setColor(new Color(13632027))
          .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
          .addField("Permissions", "You need the permission `Manage Messages` to be able to "+action+" other people's events.", false)
          .build())
        .build();
        return msg;
    }
}
