/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

/**
 *
 * @author Francis
 */
public class ContestEvent extends Event{
    public class ContestProfile{
        public final Member member;
        public ContestProfile(Member member){this.member=member;}
        public HashSet<Message> messages=new HashSet<>();
        public HashMap<Message, HashSet<Member>> votes=new HashMap<>();
        
        public int votes(Message m){
            if(!votes.containsKey(m)) return 0;
            return votes.get(m).size();
        }
        public int votes(){
            int v=0;
            v = votes.keySet().stream().map((m) -> votes.get(m).size()).reduce(v, Integer::sum);
            return v;
        }
        public void addVote(Message msg, Member m){
            if(!messages.contains(msg)) return;
            
            if(!votes.containsKey(msg)){
                votes.put(msg, new HashSet<>());
            }
            if(!votes.get(msg).contains(m)){
                votes.get(msg).add(m);
                ConcurrentHashMap<Member, Integer> v=ContestEvent.this.votes;
                if(!v.containsKey(m))
                    v.put(m, 0);
                v.put(m, v.get(m)+1);
            }
        }
        public void removeVote(Message msg, Member m){
            if(!votes.containsKey(msg)) return;
            
            if(votes.get(msg).contains(m)){
                votes.get(msg).add(m);
                ConcurrentHashMap<Member, Integer> v=ContestEvent.this.votes;
                v.put(m, v.get(m)-1);
            }
        }
        public void addMessage(Message m){
            messages.add(m);
        }
        public void removeMessage(Message m){
            messages.remove(m);
            votes.remove(m);
        }
    }
    ConcurrentHashMap<Member, Integer> votes;
    ConcurrentHashMap<Attendant, ContestProfile> profiles;
    private Date end;
    private boolean allowTextMessages;
    private int votes_user;
    
    public ContestEvent(HashMap<String, String> row, ArrayList<Attendant> attendants, ArrayList<Attendant> denials) {
        super(row, attendants, denials);
        
    }
    
    public int totalVotes(Member m){
        return votes.get(m);
    }
    
    public ContestEvent(Guild guild, Member creator) {
        super(guild, creator);
        profiles=new ConcurrentHashMap<>();
        votes=new ConcurrentHashMap<>();
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public boolean textMessagesAllowed() {
        return allowTextMessages;
    }

    public void setAllowTextMessages(boolean allowTextMessages) {
        this.allowTextMessages = allowTextMessages;
    }

    public int getVotes_user() {
        return votes_user;
    }

    public void setVotes_user(int votes_user) {
        this.votes_user = votes_user;
    }
    
    
    
}
