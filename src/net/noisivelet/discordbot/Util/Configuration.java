/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.exception;

/**
 *
 * @author Francis
 */
public final class Configuration {
    private Guild g;
    private MessageChannel auto_publish;
    private MessageChannel contest;

    public MessageChannel getContest() {
        return contest;
    }

    public void setContest(MessageChannel contest) {
        this.contest = contest;
        try {
            Database.updateConfig(this, Database.ConfigField.CONTEST);
        } catch (SQLException ex) {
            exception(ex);
        }
    }

    public Configuration(Guild g, MessageChannel auto_publish, MessageChannel contest) {
        this.g=g;
        this.auto_publish = auto_publish;
        this.contest=contest;
    }
    public Configuration (Guild g){
        this.g=g;
        auto_publish=null;
        contest=null;
    }
    public Configuration(HashMap<String, String> hm){
        this(
                DiscordBot.jda.getGuildById(hm.get("guild_id")),
                hm.get("auto_publish")==null?null:DiscordBot.jda.getGuildById(hm.get("guild_id")).getTextChannelById(hm.get("auto_publish")),
                hm.get("contest")==null?null:DiscordBot.jda.getGuildById(hm.get("guild_id")).getTextChannelById(hm.get("contest"))
        );
    }

    public MessageChannel getAuto_publish() {
        return auto_publish;
    }
    
    public Guild getGuild(){
        return g;
    }
    public void addToDatabase() throws SQLException {
        Database.queryInsert("INSERT INTO `config` VALUES(?,?,?)", g.getId(), auto_publish==null?null:auto_publish.getId(), contest==null?null:contest.getId());
    }

    public void setAuto_publish(MessageChannel auto_publish) {
        this.auto_publish = auto_publish;
        try {
            Database.updateConfig(this, Database.ConfigField.AUTO_PUBLISH);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Configuration))
            return false;
        Configuration other=(Configuration)o;
        if(this.g.getIdLong()!=other.g.getIdLong())
            return false;
        if(this.auto_publish.getIdLong()!=other.auto_publish.getIdLong())
            return false;
        return this.contest.getIdLong() == other.contest.getIdLong();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.g.getIdLong());
        hash = 53 * hash + Objects.hashCode(this.auto_publish.getIdLong());
        hash = 53 * hash + Objects.hashCode(this.contest.getIdLong());
        return hash;
    }
}
