/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Util;

import net.noisivelet.discordbot.DiscordBot;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import static net.noisivelet.discordbot.DiscordBot.error;
import net.noisivelet.discordbot.Util.Event.Attendant;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import static net.noisivelet.discordbot.DiscordBot.exception;

/**
 *
 * @author Francis
 */
public class Database {
    static private final MysqlDataSource DATASOURCE=getDataSource();
    
    public static enum ConfigField{
        AUTO_PUBLISH, CONTEST
    }
    public static enum EventField{
        NAME, DESCRIPTION, DATE, ATTENDANTS, MESSAGE, PERIODIC, EMPTY_SPOTS, ROLE
    }
    static public final boolean ok(){
        return DATASOURCE!=null;
    }
    
    static public final ConcurrentHashMap<Guild, HashMap<Member, EventCreationManager>> EVENTCREATE_MEMORY=new ConcurrentHashMap<>();
    static public ConcurrentHashMap<Guild, ArrayList<Event>> EVENTS=new ConcurrentHashMap<>();
    public static ConcurrentHashMap<Guild, Configuration> config=new ConcurrentHashMap<>();
    private static MysqlDataSource getDataSource(){
        MysqlDataSource ds=new MysqlDataSource();
        String[] params=new String[3];
        int i=0;
        File file = new File("sql.discordbot"); 
        Scanner sc; 
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException ex) {
            error("The SQL file doesn't exist.");
            try {
                file.createNewFile();
            } catch (IOException ex1) {
                DiscordBot.logger.log(Level.SEVERE, "The SQL file couldn't be created.", ex1);
            }
            return null;
        }
  
        while (sc.hasNextLine()) {
            params[i++]=sc.nextLine();
        }
        if(i!=3){
            System.err.println("The SQL file is not valid.");
            return null;
        }
        ds.setUser(params[0]);
        ds.setPassword(params[1]);
        ds.setUrl("jdbc:"+params[2]);
        return ds;
    }
    public static ArrayList<HashMap<String, String>> querySelect(String statement, String... params) throws SQLException{
        try(
                Connection cn=DATASOURCE.getConnection();
                PreparedStatement stmt=cn.prepareStatement(statement);
                ){
                    for(int i=1;i<=params.length;i++){
                        stmt.setString(i, params[i-1]);
                    }
                    ResultSet rs=stmt.executeQuery();
                    ResultSetMetaData rsmd=rs.getMetaData();
                    ArrayList<HashMap<String,String>> al=new ArrayList<>();
                    HashMap<String, String> hm;
                    while(rs.next()){
                        hm=new HashMap<>();
                        for(int i=1;i<=rsmd.getColumnCount();i++){
                            hm.put(rsmd.getColumnLabel(i), rs.getString(i));
                        }
                        al.add(hm);
                    }
                    return al;
        }
    }
    public static int queryDML(String statement, String... params) throws SQLException{
        try (
                Connection cn=DATASOURCE.getConnection();
                PreparedStatement stmt=cn.prepareStatement(statement);
                
            )
        {
            for (int i=1;i<=params.length;i++) {
                stmt.setString(i, params[i-1]);
            }
            return stmt.executeUpdate();         
        }
    }
    public static long queryInsert(String statement, String... params) throws SQLException{
        try(
                Connection cn=DATASOURCE.getConnection();
                PreparedStatement stmt=cn.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);
                )
        {
            for(int i=1;i<=params.length;i++){
                stmt.setString(i, params[i-1]);
            }
            int numRows=stmt.executeUpdate();
            if(numRows==0) throw new SQLException("Error: No rows were affected.");
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return (generatedKeys.getLong(1));
                }
                else {
                    return -1;
                }
            }
            
        }
    }

    static public void loadEvents() throws SQLException {
        ConcurrentHashMap<Guild, ArrayList<Event>> hm=EVENTS;
        ArrayList<HashMap<String, String>> query=querySelect("SELECT * FROM events");
        for(HashMap<String, String> row : query){
            ArrayList<HashMap<String, String>> q_attendants=querySelect("SELECT * FROM event_attendants WHERE id_event=?", row.get("id"));
            ArrayList<Attendant> members=new ArrayList<>(), denials=new ArrayList<>();
            Guild g=DiscordBot.jda.getGuildById(row.get("guild"));
            if(g==null){
                error("Error loading guild with ID "+row.get("guild")+": Doesn't exist.");
            }
            if(!hm.containsKey(g)){
                hm.put(g, new ArrayList<>());
            }
            q_attendants.forEach((row2) -> {
                Member me=g.getMemberById(row2.get("attendant"));
                long epoch=Long.parseLong(row2.get("joindate"));
                int type=Integer.parseInt(row2.get("denial"));
                Date d=new Date(epoch);
                if(type==Event.ATTENDANTS){
                    members.add(new Attendant(me, d));
                } else {
                    denials.add(new Attendant(me, d));
                }
            });
            members.sort(null);
            Event e=new Event(row, members, denials);
            hm.get(g).add(e);
        }
    }
    
    static public void updateEvent(Event e, EventField type) throws SQLException{
        String id=e.getDBId()+"";
        switch(type){
            case NAME:
                queryDML("UPDATE `events` SET name=? WHERE id=?", e.getName(), id);
                return;
            case DESCRIPTION:
                queryDML("UPDATE `events` SET description=? WHERE id=?", e.getDescription(), id);
                return;
            case DATE:
                queryDML("UPDATE `events` SET start=? WHERE id=?", e.getStart().getTime()+"", id);
                return;
            case ATTENDANTS:
                queryDML("UPDATE `events` SET n_attendants=? WHERE id=?", e.getN_attendants()+"", id);
                return;
            case MESSAGE:
                queryDML("UPDATE `events` SET message=?, message_channel=? WHERE id=?", e.getPublishedMessage().getId(), e.getPublishedMessage().getChannel().getId(), id);
                return;
            case PERIODIC:
                queryDML("UPDATE `events` SET periodic=?, offsetPeriodic=? WHERE id=?", e.isPeriodic()?"1":"0", e.isPeriodic()?e.offsetPeriodic()+"":null, e.getDBId()+"");
                return;
            case EMPTY_SPOTS:
                queryDML("UPDATE `events` SET showEmptySpots=? WHERE id=?", e.showEmptySpots()?"1":"0", e.getDBId()+"");
                return;
            case ROLE:
                queryDML("UPDATE `events` SET role=? WHERE id=?", e.getRole().getId(), e.getDBId()+"");

        }
    }
    
    static public void updateConfig(Configuration c, ConfigField type) throws SQLException{
        switch(type){
            case AUTO_PUBLISH:
                queryDML("UPDATE `config` SET auto_publish=? WHERE guild_id=?", c.getAuto_publish()==null?null:c.getAuto_publish().getId(), c.getGuild().getId());
            case CONTEST:
                queryDML("UPDATE `config` SET contest=? WHERE guild_id=?", c.getContest()==null?null:c.getContest().getId(), c.getGuild().getId());
        }
    }
    
    static public void loadConfig() throws SQLException{
        ArrayList<HashMap<String, String>> hm=querySelect("SELECT * FROM config");
        for(HashMap<String, String> row:hm){
            Configuration c=new Configuration(row);
            config.put(DiscordBot.jda.getGuildById(row.get("guild_id")),c);
        }
    }
    
    static public Configuration getConfig(Guild guild){
        Configuration c;
        if(!config.containsKey(guild)){
            c=new Configuration(guild);
            try {
                c.addToDatabase();
            } catch (SQLException ex) {
                exception(ex);
            }
            config.put(guild,c);
        } else {
            c=config.get(guild);
        }
        
        return c;
        
    }
    
    static public EventCreationManager getEventCreationManager(Member m){
        if(!EVENTCREATE_MEMORY.containsKey(m.getGuild())){
            EVENTCREATE_MEMORY.put(m.getGuild(), new HashMap<>());
        }
        EventCreationManager manager=EVENTCREATE_MEMORY.get(m.getGuild()).get(m);
        return manager;
    }
    
    static public EventCreationManager newEventCreationManager(Member m){
        Guild g=m.getGuild();
        if(!EVENTCREATE_MEMORY.containsKey(g)){
            EVENTCREATE_MEMORY.put(g, new HashMap<>());
        }
        EventCreationManager ecm=new EventCreationManager(m);
        EVENTCREATE_MEMORY.get(g).put(m, ecm);
        return ecm;
    }
}
