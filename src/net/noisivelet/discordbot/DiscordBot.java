/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot;


import net.noisivelet.discordbot.Util.Configuration;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Listeners.MemberLeaveManager;
import net.noisivelet.discordbot.Tasks.MessageDeleter;
import net.noisivelet.discordbot.Listeners.MessageManager;
import net.noisivelet.discordbot.Listeners.ReactionManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

/**
 * 
 * @author Francis
 */
public class DiscordBot{
    static{
        System.setProperty("java.util.logging.SimpleFormatter.format",
              "[%1$tF %1$tT] [%4$-7s] %5$s %n");
    }
    public static void log(String str){logger.log(Level.INFO, str);}
    public static void error(String str){logger.log(Level.SEVERE, str);}
    public static void warning(String str){logger.log(Level.WARNING, str);}
    public static final Logger logger=Logger.getLogger("DiscordBot");
    public static final ExecutorService EXECUTOR=Executors.newCachedThreadPool();
    public static final ScheduledExecutorService SCHEDULER=Executors.newScheduledThreadPool(8);
    public static long CREATOR_ID=178860287292735489L;
    public static JDA jda;
    public static String version="Noisivelet EventBot 1.0";
    private static JDA buildJDA(){
        JDA _jda;
        File tokenFile=new File("token.discordbot");
        
        if (!tokenFile.exists() || !tokenFile.isFile()){
            warning("The token file \"token.discordbot\" doesn't exist. Generating a new one.");
            try {
                tokenFile.createNewFile();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Error creating new file: "+ex.getMessage(), ex);
            }
            return null;
        }
        
        if(!tokenFile.canRead()){
            error("The token file \"token.discordbot\" can't be read.");
            return null;
        }
        
        if(tokenFile.length() == 0){
            error("The token file \"token.discordbot\" is not a valid token file.");
            return null;
        }
        
        String token=loadTokenFromFile(tokenFile);
        
        try {
            _jda=new JDABuilder(AccountType.BOT).setToken(token).build().awaitReady();
        } catch (LoginException | InterruptedException ex) {
            logger.log(Level.SEVERE, "Error while trying to connect Discord servers.", ex);
            return null;
        }
        
        return _jda;
    }
    
    private static String loadTokenFromFile(File file){
        try (BufferedReader is = new BufferedReader(new FileReader(file))) {    
            return is.readLine();
        } catch (FileNotFoundException ex) {
            exception(ex);
            return null;
        } catch (IOException ex){
            exception(ex);
            return null;
        }
    }
    
    public static void main(String[] args) throws InterruptedException, LoginException {
       if(!Database.ok()) return;
       jda=buildJDA();
       if (jda==null) return;
       int totalguilds=jda.getGuilds().size();
       System.out.println("---------------------------------\n");
       log(version);
       log("Bot operating in "+totalguilds+" Discord servers.");
        try {
            Database.loadEvents();
            Database.loadConfig();
        } catch (SQLException ex) {
            exception(ex);
            return;
        }
       jda.addEventListener(new MessageManager());
       jda.addEventListener(new ReactionManager(Database.EVENTS));
       jda.addEventListener(new MemberLeaveManager());
       log("All ready");
    }
    
    public static void exception(Throwable ex){
        error("One of the EventListeners caught an exception.");
        error("Exception message: "+ex.getMessage()+"\nCaused by: "+ex.getCause());
        for(StackTraceElement e : ex.getStackTrace()){
            error("\tat "+e.getClassName()+ "("+e.getFileName()+":"+e.getLineNumber()+")");
        }
    }
}
