/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Command;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class Help extends CommandExecutor{

    @Override
    void run(Message message, String[] params, Member m, MessageChannel channel) {
        MessageBuilder msgBuilder=new MessageBuilder();
        EmbedBuilder embedBuilder=new EmbedBuilder()
            .setDescription("[This bot's source code](https://gitlab.com/Noisivelet/discord-event-bot) \n*Arguments enclosed inbetween <> are __required__.\nArguments enclosed inbetween [] are __optional__.*")
            .setColor(new Color(15068682))
            .setFooter("United as One - EventBot | Made by "+message.getJDA().getUserById(DiscordBot.CREATOR_ID).getAsMention(), "https://discordapp.com/assets/aef26397c9a6a3afee9c857c5e6f3317.svg")
            .setThumbnail("https://cdn.discordapp.com/attachments/624926218512367616/668416919967236097/UAO.png")
            .setAuthor("Help & Command reference", null, "https://cdn.discordapp.com/attachments/624926218512367616/668416919967236097/UAO.png");
        
        Utils.COMMAND_LIST.stream().filter((c) -> (!c.isInternal())).forEachOrdered((c) -> {
            String desc="";
            if(!c.getArgs().equals("")){
                desc+="__Arguments__: `"+c.getArgs()+"`\n";
            }
            desc+= "__Usage__: "+c.getUsage()+"\n";
            String aliases="";
            Object[] aliases_str=c.getAliases();
            if(aliases_str.length!=0){
                for(int i=0;i<aliases_str.length;i++){
                    aliases+=aliases_str[i];
                    if(i<=aliases_str.length-1){
                        aliases+=", ";
                    }
                }
                desc+="__Aliases__: "+aliases;
            }
            
            embedBuilder.addField("**"+c.getCommand()+"**", desc, false);
        });
        msgBuilder.setEmbed(embedBuilder.build());
        Utils.sendAndDelete(msgBuilder.build(), channel, 90);
    }
    
}
