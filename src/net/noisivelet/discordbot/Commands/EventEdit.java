/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.log;
import static net.noisivelet.discordbot.Util.Utils.errorMessage;
import static net.noisivelet.discordbot.Util.Utils.successMessage;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class EventEdit extends CommandExecutor {

    @Override
    public void run(Message message, String[] args, Member m, MessageChannel channel) {
        if(args.length<3){
            Utils.sendAndDelete(incorrectParametersEdit(args,m), channel, 30);
            return;
        }
        int index;
        try{
            index=Integer.parseInt(args[1]);
        } catch (NumberFormatException e){
            Utils.sendAndDelete(incorrectParametersEdit(args,m), channel, 30);
            return;
        }
        
        if(index<0||Database.EVENTS.get(m.getGuild()).size() <=index){
            Utils.sendAndDelete(incorrectParametersEdit(args,m), channel, 30);
            return;
        }
        
        Event e=Database.EVENTS.get(m.getGuild()).get(index);
        if(!e.canDelete(m)){
            Utils.sendAndDelete(Utils.insufficientPermissionsEdit(m, "edit"), channel, 10);
            return;
        }
        switch(args[2]){
            case "name":
            case "title":
                name(args,m,channel,e);
                return;
                
            case "desc":
            case "description":
                desc(args,m,channel,e);
                return;
                
            case "date":
            case "start":
            case "startdate":
            case "datestart":
                date(args,m,channel,e);
                return;
                
            case "attendants":
            case "members":
            case "capacity":
            case "users":
                users(args,m,channel,e);
                return;
                
            case "repeat":
            case "periodic":
                periodic(args,m,channel,e);
                return;
                
            case "emptyslots":
            case "showemptyslots":
            case "slots":
                slots(args,m,channel,e);
                return;
        }
    }
    
    private Message incorrectParametersEdit(String[] params, Member m){
        String errormsg="Command failed due to incorrect argument number. Message: ";
        for(String arg : params) errormsg += arg + " ";
        log(errormsg);
        
        Message msg=new MessageBuilder()
        .setEmbed(new EmbedBuilder()
          .setTitle("Error executing command")
          .setDescription("The command couldn't be executed due to incorrect parameters.\nThat can be due to an incorrect number of parameters or an incorrect ID for the event.")
          .setColor(new Color(13632027))
          .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
          .addField("Command syntax:", "`!eventedit <id> <parameter> [new value]`\n\n*Replace <id> with the ID of the event you want to edit. If you don't remember the ID, you can use the command `!eventlist` to get a list of all created events.\nFor more information about the command, type `!uhelp` in chat.*", true)
          .build())
        .build();
        return msg;
    }

    private void name(String[] args, Member m, MessageChannel c, Event e) {
        if(args.length<4){
            Utils.sendAndDelete(incorrectParametersEdit(args, m), c, 10);
            return;
        }

        String title="";
        for(int i=3;i<args.length;i++){
            title+=args[i];
            if(i!=args.length-1)
                title+=" ";
        }
        e.setName(title);
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" changed the name of event id "+e.getDBId()+ "to "+title);

        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing event")
            .setDescription("Name of the event successfully changed.")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, c, 10);
    }

    private void desc(String[] args, Member m, MessageChannel channel, Event e) {
        String title="";
        if(args.length<4){
            Utils.sendAndDelete(incorrectParametersEdit(args, m), channel, 10);
            return;
        }

        title="";
        for(int i=3;i<args.length;i++){
            title+=args[i];
            if(i!=args.length-1)
                title+=" ";
        }
        e.setDescription(title);
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" edited the description of event"+e.getName());
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing event")
            .setDescription("Description of the event correctly changed.")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, channel, 10);
    }

    private void date(String[] args, Member m, MessageChannel channel, Event e) {
        if(args.length<5){
            Utils.sendAndDelete(incorrectParametersEdit(args,m), channel, 10);
            return;
        }
        String date=args[3]+" "+args[4];
        
        Date d;
        try {
            d=new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(date);
        } catch (ParseException ex) {
            Utils.sendAndDelete(errorMessage("Editing event - Incorrect date", "The date specified is not in the valid format.\n\n**The date has to be typed using the following format:\n`DD-MM-YYYY HH:mm`**\n\n*For example: 23-02-2020 19:00 would equal to the 23th of February, 2020 at 19:00 (Server time)*",m), channel, 10);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used an invalid date. Their format: "+date);
            return;
        }
        
        long now=Instant.now().getEpochSecond()*1000;
        if(d.getTime() < now){
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used a date before now.");
            Utils.sendAndDelete(errorMessage("Editing event - Incorrect date", "The start date should be after now. The specified date has already passed.\n\n**Type the date and time, using the following format:\n`DD-MM-YYYY HH:mm`**\n\n*For example: 23-02-2020 19:00 would equal to the 23th of February, 2020 at 19:00 (Server time)*",m), channel, 15);
            return;
        }
        e.setStart(d);
        
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing event")
            .setDescription("Start date succesfully recorded.")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, channel, 10);
        return;
    }

    private void users(String[] args, Member m, MessageChannel channel, Event e) {
        if(args.length<4){
            Utils.sendAndDelete(incorrectParametersEdit(args, m), channel, 10);
            return;
        }
        int a;
        String attendants=args[3];
        try{
            a=Integer.parseInt(attendants);
        } catch(NumberFormatException ex){
            Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                .setTitle("Creating new event - Attendants")
                .setDescription("Error parsing attendant number. Try again.\n\n**Type (with a number), the maximum number of attendants this event will have.**")
                .setColor(new Color(13632027))
                .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" typed an invalid number. Their argument: "+attendants);
            Utils.sendAndDelete(msg, channel, 10);
            return;
        }

        if(a==0||a<-1){
            Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                .setTitle("Creating new event - Attendants")
                .setDescription("Error parsing attendant number: An event must have, at the very least, space for a member.\n\n**Type (with a number), the maximum number of attendants this event will have.**")
                .setColor(new Color(13632027))
                .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used an invalid number of members. Their number of members: "+attendants);
            Utils.sendAndDelete(msg, channel, 10);
            return;
        }
        e.setN_attendants(a);
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" changed the number of attendants for the event "+e.getName()+" to "+attendants);
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing event")
            .setDescription("Number of attendants succesfully updated.")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, channel, 10);
    }

    private void periodic(String[] args, Member m, MessageChannel channel, Event e) {
        Message msg;
        if(args.length<4 || args[3].equals("-1")){
            e.setPeriodic(-1);
            msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                .setTitle("Editing event")
                .setDescription("The event is no longer periodic.")
                .setColor(new Color(4886754))
                .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" made the event \""+e.getName()+"\" no longer periodic.");
            Utils.sendAndDelete(msg, channel, 10);
        } else {
            if(args[3].length() == 1){
                msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                    .setTitle("Editing event - Incorrect time unit")
                    .setDescription("Error parsing arguments: There is no time unit.\nType a number followed with a time unit, without spaces.")
                    .setColor(new Color(13632027))
                    .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                    .addField("Time units", "`h` - hours\n`d` - days\n`w` - weeks", true)
                    .addField("Examples", "`3h` - 3 hours\n`5d` - 5 days\n `2w` - 2 weeks", true)
                    .build())
                .build();
                Utils.sendAndDelete(msg, channel, 20);
                return;
            }
            char unit='\000';
            int pivot=-1;
            for(int i=1;i<args[3].length();i++){
                if(!Character.isDigit(args[3].charAt(i))){
                    unit=args[3].charAt(i);
                    pivot=i;
                }
            }
            if(unit=='\000'){
                msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                    .setTitle("Editing event - Incorrect time unit")
                    .setDescription("Error parsing arguments: There is no time unit.\nType a number followed with a time unit, without spaces.")
                    .setColor(new Color(13632027))
                    .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                    .addField("Time units", "`h` - hours\n`d` - days\n`w` - weeks", true)
                    .addField("Examples", "`3h` - 3 hours\n`5d` - 5 days\n `2w` - 2 weeks", true)
                    .build())
                .build();
                Utils.sendAndDelete(msg, channel, 20);
                return;
            }
            String number="";
            for(int i=0;i<pivot;i++){
                number+=args[3].charAt(i);
            }
            long n;
            try{
                n=Long.parseLong(number);
            } catch (NumberFormatException ex){
                msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                    .setTitle("Editing event - Incorrect number")
                    .setDescription("Error parsing arguments: The number is incorrect.\nType a number followed with a time unit, without spaces.")
                    .setColor(new Color(13632027))
                    .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                    .addField("Time units", "`h` - hours\n`d` - days\n`w` - weeks", true)
                    .addField("Examples", "`3h` - 3 hours\n`5d` - 5 days\n `2w` - 2 weeks", true)
                    .build())
                .build();
                Utils.sendAndDelete(msg, channel, 20);
                return;
            }

            switch(unit){
                case 'w':
                    n*=7;
                case 'd':
                    n*=24;
                case 'h':
                    n*=3600000;
                    break;
                default:
                    msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                    .setTitle("Editing event - Incorrect time unit")
                    .setDescription("Error parsing arguments: The time unit specified is incorrect.\nType a number followed with a time unit, without spaces.")
                    .setColor(new Color(13632027))
                    .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                    .addField("Time units", "`h` - hours\n`d` - days\n`w` - weeks", true)
                    .addField("Examples", "`3h` - 3 hours\n`5d` - 5 days\n `2w` - 2 weeks", true)
                    .build())
                .build();
                Utils.sendAndDelete(msg, channel, 20);
                return;
            }
            e.setPeriodic(n);
            msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                .setTitle("Editing event")
                .setDescription("The event is now periodic and will repeat when finished. That's marked with :recycle: in the `!eventlist` window.")
                .setColor(new Color(4886754))
                .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                .build())
            .build();
            Utils.sendAndDelete(msg, channel, 10);
        }
        return;
    }

    private void slots(String[] args, Member m, MessageChannel channel, Event e) {
        e.setShowEmptySpots(!e.showEmptySpots());
        String message;
        if(e.showEmptySpots()){
            message="The event will now show empty slots on its event card.";
        } else {
            message="The event will no longer show empty slots on its event card.";
        }
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing event")
            .setDescription(message)
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, channel, 10);
    }
}
