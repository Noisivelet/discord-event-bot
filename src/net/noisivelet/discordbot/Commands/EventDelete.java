/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import java.sql.SQLException;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class EventDelete extends CommandExecutor {

    @Override
    public void run(Message message, String[] args, Member m, MessageChannel channel) {
        if(args.length!=2){
            Utils.sendAndDelete(incorrectParametersDelete(m), channel, 30);
            return;
        }
        int i;
        try{
            i=Integer.parseInt(args[1]);
        } catch(NumberFormatException e){
            Utils.sendAndDelete(incorrectParametersDelete(m), channel, 30);
            return;
        }
        if(i>=Database.EVENTS.get(m.getGuild()).size()||i<0){
            Utils.sendAndDelete(incorrectParametersDelete(m), channel, 30);
            return;
        }
        Event e=Database.EVENTS.get(m.getGuild()).get(i);
        if(!e.canDelete(m)){
            Utils.sendAndDelete(Utils.insufficientPermissionsEdit(m, "delete"), channel, 10);
            return;
        }
        e.delete();
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Event deleted")
            .setDescription("I've deleted the event.\n\n**Have in consideration:** The ID of the rest of the events may have changed. Use the command `!eventlist` to check.")
            .setColor(new Color(8311585))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
          .build();
        Utils.sendAndDelete(msg, channel, 10);
    }
    
    public Message incorrectParametersDelete(Member m){
        Message msg=new MessageBuilder()
        .setEmbed(new EmbedBuilder()
          .setTitle("Error executing command")
          .setDescription("The command couldn't be executed due to incorrect parameters.\nThat can be due to an incorrect number of parameters or an incorrect ID for the event.")
          .setColor(new Color(13632027))
          .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
          .addField("Command syntax:", "`!eventdelete <id>`\n\n*Replace <id> with the ID of the event you want to delete. If you don't remember the ID, you can use the command `!eventlist` to get a list of all created events.*", true)
          .build())
        .build();
        return msg;
    }
}
