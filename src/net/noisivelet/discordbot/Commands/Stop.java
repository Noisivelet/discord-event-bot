/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.warning;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class Stop extends CommandExecutor {

    @Override
    public void run(Message message, String[] params, Member m, MessageChannel channel) {
        if(m.getUser().getIdLong()!=DiscordBot.CREATOR_ID){
            Message msg=new MessageBuilder()
            .setEmbed(new EmbedBuilder()
              .setTitle("Error executing command")
              .setDescription("Only Noisivelet can shut me down.")
              .setColor(new Color(13632027))
              .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
              .build())
            .build();
            Utils.sendAndDelete(msg, channel, 10);
            return;
        }
        channel.sendMessage("Shuting down for maintanance. BRB.").complete();
        warning("Received STOP signal from "+m.getUser().getName()+"#"+m.getUser().getDiscriminator()+", which has been recognized as BOT_OWNER. Stopping the bot...");
        DiscordBot.jda.shutdown();
        System.exit(0);
    }
    
}
