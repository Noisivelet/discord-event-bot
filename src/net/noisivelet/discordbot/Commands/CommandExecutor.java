/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;

/**
 *
 * @author Francis
 */
public abstract class CommandExecutor {
    public final void execute(Message message, String[] params, Member m, MessageChannel channel){
        run(message,params,m,channel);
        after(message,channel);
    }
    abstract void run(Message message, String[] params, Member m, MessageChannel channel);
    void after(Message message, MessageChannel channel){
        message.delete().submit();
    }
}
