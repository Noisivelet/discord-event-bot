/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.exception;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.EventCreationManager;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class EventCreate extends CommandExecutor{

    @Override
    public void run(Message message, String[] params, Member m, MessageChannel channel) {
        if(!Event.canCreate(m)){
            Message msg=insufficientPermissions(m);
            Utils.sendAndDelete(msg, channel, 30);
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" has insufficient permissions to create a new event.");
            return;
        }
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" started the creation of a new event.");
        EventCreationManager ecm=Database.newEventCreationManager(m);
        
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Creating new event - Type selection")
            .setDescription("Let's begin, then. Lets determine the type of event.")
            .addField("Event types", ":one: Normal event: Starts at a given date and has a determined number of users.\n:two: Contest event: Only one can be active at the same time. Starts and ends at given dates; each user can participate and vote for some messages in a given channel.", false)
            .addField("What you need to do:", "**__Type, in chat, the number of the type of event you want to create, or the name of the event to automatically create a normal event.__**", false)
            .setColor(new Color(4886754))
            .setFooter("United as One EventBot | Created by @Noisivelet", "https://cdn.discordapp.com/icons/594802818532114469/9d4e19c1bc55c8d78c5001f919b53067.png")
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .addField("Have in consideration", "*You won't be able to chat until we've finished. You can stop this process and delete the event by typing* `cancel` *in chat at any time.*", false)
            .build())
        .build();
        ecm.addMessage(channel.sendMessage(msg).complete());
        ecm.addMessage(message);
    }
    
    @Override
    void after(Message m, MessageChannel c){}
    
    public Message insufficientPermissions(Member m){
        return new MessageBuilder()
        .setEmbed(new EmbedBuilder()
          .setTitle("Error executing command")
          .setDescription("You lack the permissions to create an event.")
          .setColor(new Color(13632027))
          .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
          .addField("Permissions", "You need the permission `Mention Everyone` to be able to create events.", false)
          .build())
        .build();
    }
}