/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class UpdateCards extends CommandExecutor {

    @Override
    void run(Message message, String[] params, Member m, MessageChannel channel) {
        if(m.getUser().getIdLong()!=DiscordBot.CREATOR_ID){
            Message msg=new MessageBuilder()
            .setEmbed(new EmbedBuilder()
              .setTitle("Error executing command")
              .setDescription("Only one of the bot developers can issue this command.")
              .setColor(new Color(13632027))
              .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
              .build())
            .build();
            Utils.sendAndDelete(msg, channel, 10);
            return;
        }
        Database.EVENTS.get(m.getGuild()).forEach((e) -> {
            e.updatePublishedMessage();
        });
        
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Updating...")
            .setDescription("All event cards in this server have been updated.")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        Utils.sendAndDelete(msg, channel, 10);
    }
    
}
