/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class EventList extends CommandExecutor{

    @Override
    void run(Message message, String[] params, Member m, MessageChannel channel) {
        String descc="Here's a list of all events that have been planned but haven't started yet.";
        if(m.getUser().getIdLong()==DiscordBot.CREATOR_ID){
            descc+="\nDetected the UUID of a developer; added debug options to each listed event.";
        }
        EmbedBuilder builder=new EmbedBuilder()
            .setDescription(descc)
            .setColor(new Color(5301186))
            .setFooter("United as One EventBot | Created by @Noisivelet#0001", "https://cdn.discordapp.com/attachments/624926218512367616/668416919967236097/UAO.png")
            .setThumbnail("https://cdn.discordapp.com/attachments/624926218512367616/668416919967236097/UAO.png")
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl());
        ConcurrentHashMap<Guild, ArrayList<Event>> events=Database.EVENTS;
        for(int i=0;i<events.get(m.getGuild()).size();i++){
            Event e=events.get(m.getGuild()).get(i);
            String name;
            String desc="";
            if(!e.canBePublished()){
                name="**<event under construction>**";
                desc+="**Event ID:** "+i+"\n";
                desc+="**Creator:** "+e.creator.getAsMention();
            } else {
                name="**"+e.getName()+"**";
                String date=new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.UK).format(e.getStart());
                String hour=new SimpleDateFormat("HH:mm").format(e.getStart());
                desc+="**Event ID:** "+i+"\n";
                desc+="**Attendants / Denials:** ["+e.getAttendants().size()+"/"+((e.getN_attendants()==-1)?"∞":e.getN_attendants())+"] / "+e.getDenials().size()+"\n";
                desc+="**Starting:** "+date+" "+hour+"\n";
                desc+="**Creator:** "+e.creator.getAsMention()+"\n";
                desc+="**Showing empty slots?:** "+e.showEmptySpots();
                if(e.isPublished()){
                    desc+="\n**Event card message:** [Click here](https://discordapp.com/channels/"+e.guild.getId()+"/"+e.getPublishedMessage().getChannel().getId()+"/"+e.getPublishedMessage().getId()+")";
                    if(m.getUser().getIdLong()==DiscordBot.CREATOR_ID){
                        desc+="\n**[Dev] Message ID:** "+e.getPublishedMessage().getId();
                        desc+="\n**[Dev] Starting (Epoch):** "+e.getStart().getTime();
                        if(e.isPeriodic()){
                            desc+="\n**[Dev] Restarting offset(Epoch):** "+e.offsetPeriodic();
                        }
                    }
                }
                
            }
            if(e.isPeriodic()){
                name+=" :recycle:";
            }
            builder.addField(name, desc, false);
            
        }
        Message msg=new MessageBuilder().setEmbed(builder.build()).build();
        Utils.sendAndDelete(msg, channel, 60);
    }
    
}
