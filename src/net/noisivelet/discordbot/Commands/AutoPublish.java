/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.noisivelet.discordbot.DiscordBot;
import static net.noisivelet.discordbot.DiscordBot.log;
import net.noisivelet.discordbot.Util.Configuration;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class AutoPublish extends CommandExecutor{

    @Override
    void run(Message message, String[] params, Member m, MessageChannel channel) {
        if(!Event.hasFullPermissions(m)){
            Message msg=new MessageBuilder()
            .setEmbed(new EmbedBuilder()
              .setTitle("Error executing command")
              .setDescription("You lack the permissions to change the global configuration.")
              .setColor(new Color(13632027))
              .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
              .addField("Permissions", "You need the permission `Manage Messages` to be able to edit this server's configuration for the bot.", false)
              .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" lacks permissions to perform the command.");
            Utils.sendAndDelete(msg, channel, 10);
        }
        Configuration config=Database.getConfig(m.getGuild());
        String[] args=message.getContentRaw().split(" ");
        if(args.length==1){
            config.setAuto_publish(null);
            Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
                .setTitle("Editing configuration")
                .setDescription("Auto-advertise disabled successfully.")
                .setColor(new Color(4886754))
                .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
                .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" disabled auto-advertise for guild "+m.getGuild().getName());
            Utils.sendAndDelete(msg, channel, 10);
            return;
        }
        
        List<TextChannel> channels=message.getMentionedChannels();
        if(channels.size()!=1){
            Message msg=new MessageBuilder()
            .setEmbed(new EmbedBuilder()
              .setTitle("Error executing command")
              .setDescription("Incorrect syntax.")
              .setColor(new Color(13632027))
              .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
              .addField("Syntax", "`!autopublish #message_channel` will update the autopublish channel.\n`!autopublish` will disable the option.", false)
              .build())
            .build();
            log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" used incorrect syntax. Their command: "+message.getContentRaw());

            Utils.sendAndDelete(msg, channel, 30);
            return;
        }
        config.setAuto_publish(channels.get(0));
        Message msg=new MessageBuilder().setEmbed(new EmbedBuilder()
            .setTitle("Editing configuration")
            .setDescription("Auto-advertise successfully set to the channel "+channels.get(0).getAsMention()+".")
            .setColor(new Color(4886754))
            .setAuthor(m.getUser().getName(), null, m.getUser().getAvatarUrl())
            .build())
        .build();
        log(m.getUser().getName()+"#"+m.getUser().getDiscriminator()+" enabled auto-advertise for guild "+m.getGuild().getName()+" to channel "+channels.get(0).getName());
        Utils.sendAndDelete(msg, channel, 10);
        return;
    }
    
}
