/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.noisivelet.discordbot.Commands;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.noisivelet.discordbot.Commands.CommandExecutor;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.discordbot.Util.Database;
import net.noisivelet.discordbot.Util.Event;
import net.noisivelet.discordbot.Util.Utils;

/**
 *
 * @author Francis
 */
public class EventPublish extends CommandExecutor {

    public EventPublish() {
    }

    @Override
    public void run(Message message, String[] args, Member m, MessageChannel channel) {
        if(args.length!=2){
            Message msg=error(m.getUser());
            Utils.sendAndDelete(msg, channel, 15);
            return;
        }
        
        int i;
        try{
            i=Integer.parseInt(args[1]);
        } catch (NumberFormatException e){
            Message msg=error(m.getUser());
            Utils.sendAndDelete(msg, channel, 15);
            return;
        }
        if(Database.EVENTS.get(m.getGuild()).size() <= i){
            Message msg=error(m.getUser());
            Utils.sendAndDelete(msg, channel, 15);
            return;
        }
        
        Event e=Database.EVENTS.get(m.getGuild()).get(i);
        if(!e.canDelete(m)){
            Utils.sendAndDelete(Utils.insufficientPermissionsEdit(m, "publish"), channel, 10);
            return;
        }
        
        e.publish(channel);
    }
    
    private Message error(User executor){
        Message m=new MessageBuilder()
        .setEmbed(new EmbedBuilder()
          .setTitle("Error executing command")
          .setDescription("The command couldn't be executed due to incorrect parameters.\nThat can be due to an incorrect number of parameters or an incorrect ID for the event.")
          .setColor(new Color(13632027))
          .setAuthor(executor.getName(), null, executor.getAvatarUrl())
          .addField("Command syntax:", "`!eventpublish <id>`\n\n*Replace <id> with the ID of the event you want to publish. If you don't remember the ID, you can use the command `!eventlist` to get a list of all created events.*", true)
          .build())
        .build();
        return m;
    }
}
